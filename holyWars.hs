
import Data.List (unfoldr)
import Text.Printf (printf)
import Control.Arrow ((&&&))

raidTime :: (Num a) => a
raidTime = 2 * 60 + 36

warTime :: (Num a) => a
warTime = 5

symbolTime :: (Num a) => a
symbolTime = 9

symbolCooldown :: Double
symbolCooldown = 15.8

warCooldownRange :: [Double]
warCooldownRange = [23.3, 23.4..40.1]

warCooldownRangeWithSum :: [(Double, Double)]
warCooldownRangeWithSum = map ((sum . evalWars) &&& id) warCooldownRange

bestWarCooldown :: (Double, [Double])
bestWarCooldown =
  foldr eval (0.0, [60.0]) warCooldownRangeWithSum
  where
    eval (current, cooldown) same@(best, cooldownRange) =
      if current > best then
        (current, [cooldown])
      else if current == best then
        (current, cooldown : cooldownRange)
      else
        same

evalWars :: Double -> [Double]
evalWars = map (eval) . relativeToSymbols

evalWarsStr :: Double -> [String]
evalWarsStr = map (printf "%.2f") . evalWars

relativeToSymbols :: Double -> [Double]
relativeToSymbols = map (/symbolCooldown) . holyWars

holyWars :: Double -> [Double]
holyWars cooldown =
  unfoldr
  (\x -> if x > raidTime then Nothing else Just (x, x + cooldown))
  cooldown

eval :: Double -> Double
eval x =
  if wastedTime >= 0 then
    max 0 (warTime - wastedTime)
  else
    min warTime (symbolTime + wastedTime)
  where
  wastedTime = (fromIntegral (round x) - x) * symbolCooldown
